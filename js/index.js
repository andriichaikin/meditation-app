"use strict"

const outline = document.querySelector(".app__moving-outline circle");
const outlineLength = outline.getTotalLength();

const song = document.querySelector(".song");
const video = document.querySelector(".bg-video");
const playBtn = document.querySelector(".play");

const timeDisplay = document.querySelector('.app__time-display');


let duration;


const app = () => {
	duration = getDefaultDuration();
	
	playBtn.addEventListener("click", () => {
		toggleSong();
	});
	
	timeDisplay.textContent = `${Math.floor(duration / 60)}:0${Math.floor(duration % 60)}`;

	calculateProgressBar();

	const durationBtn = document.querySelectorAll('.app__duration button');
	durationBtn.forEach( btn => {
		btn.addEventListener("click", function() {
			duration = this.getAttribute('data-time');
			song.pause();
			video.pause();
			playBtn.src = './svg/play.svg'
			song.currentTime = 0;
		})
	});

	toggleMediationType();
};

const getDefaultDuration = () => {
	const durationEl = document.querySelector('.app__duration button:nth-child(1)');
	const duration = durationEl.getAttribute('data-time');
	
	return duration;
};

const calculateProgressBar = function () {
	outline.style.strokeDasharray = outlineLength;
	outline.style.strokeDashoffset = outlineLength;

	song.ontimeupdate = () => {
		let currentTime = song.currentTime;

		const elapsed = duration - currentTime;
		let seconds = Math.floor(elapsed % 60);
		const minutes = Math.floor(elapsed / 60);

		const progress = outlineLength - (currentTime / duration) * outlineLength;
		outline.style.strokeDashoffset = progress;

		if(elapsed <= 0) {
			song.pause();
			video.pause();
			playBtn.src = './svg/play.svg';

			return;
		}

		if(seconds < 10) seconds = "0" + seconds

		timeDisplay.textContent = `${minutes}:${seconds}`;
	};
};

const toggleSong = () => {
	if(song.paused) {
		song.play();
		video.play();
		playBtn.src = './svg/pause.svg';
	} else {
		song.pause();
		video.pause();
		playBtn.src = './svg/play.svg';
	}
};

const toggleMediationType = () => {
	const mediaType = document.querySelectorAll('.app__meditation-types button');
	mediaType.forEach(type => {
		type.addEventListener('click', function () {
			const songPath = this.getAttribute('data-sound');
			const videoPath = this.getAttribute('data-video');

			if(song.src.indexOf(songPath) > 1) return;

			song.pause();
			song.currentTime = 0;
			video.pause();

			playBtn.src = './svg/play.svg';
			song.src = songPath;
			video.src = videoPath;
		})
	})
};

app();